#ifndef PROCESO_H // prevent multiple inclusions of header file
#define PROCESO_H

#include <string>
#include <iostream>

using namespace std;

class proceso {
  private:
    pid_t pid;
    int segundos;
    char* url_youtube;

  public:
    // Constructor
    proceso (int seg, char* url_yutube) {
      url_youtube = url_yutube;
      segundos = seg;
      void crear();
      void codigo();
    }
};
#endif